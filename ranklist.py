# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 09:51:29 2019

@author: LENOVO
"""
import operator

def get_marklist(filename):
    marklist = {line[:5]: int(line[6:9]) for line in open(filename)}
    return marklist
        
def get_ranks(filename):
    marklist = get_marklist(filename)
    sorted_marklist = dict(sorted(marklist.items(), key=operator.itemgetter(1), reverse = True))
    print("Rank\t" + "Rollnum\t" + "Mark")
    rank = 1
    while rank < len(sorted_marklist):
        if list(sorted_marklist.values())[rank-1] == list(sorted_marklist.values())[rank]:
            print("    \t", list(sorted_marklist.keys())[rank], " ", list(sorted_marklist.values())[rank])
        else:
            print(rank,"\t", list(sorted_marklist.keys())[rank], " ", list(sorted_marklist.values())[rank])
        
        rank += 1
        
get_ranks("marklist.txt")
